package br.com.meubolsoemdia.jimbopoc.screen.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.meubolsoemdia.jimbopoc.R
import br.com.meubolsoemdia.jimbopoc.Util
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.IOException

/**
 * Created by Éverdes Soares on 06/01/2020.
 */

class MainActivity : AppCompatActivity() {

    companion object {
        const val FILENAME_BIBLE = "JimboDB.db"
    }

    private val mViewModel: MainViewModel by viewModel()

    private val mAdapter: CardAdapter by lazy {
        CardAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initObservers()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        initView()
        moveDatabase()
        mViewModel.fetchCards()
    }

    private fun initView() {
        recyclerView?.let {
            it.adapter = mAdapter
            it.layoutManager = LinearLayoutManager(this)
        }
    }

    private fun initObservers() {
        mViewModel.cards.observe(this, Observer { cards ->
            mAdapter.addData(cards)
        })
    }

    private fun moveDatabase() {
        Runnable {
                try {
                    val pathOriginDatabaseFile = assets.open(FILENAME_BIBLE)
                    val pathDestinationDatabaseFile = getDatabasePath(FILENAME_BIBLE).absolutePath
                    Util.moveFile(pathOriginDatabaseFile, pathDestinationDatabaseFile)

//                    Preferences(ctx).saveBibleDownloaded(true)

//                    showSuccess()
                } catch (e: IOException) {
                    e.printStackTrace()
//                    showMessage("Ocorreu um erro ao copiar o banco de dados")
                }
        }.run()
    }
}