package br.com.meubolsoemdia.jimbopoc

import androidx.room.TypeConverter
import java.math.BigDecimal

/**
 * Created by Éverdes Soares on 06/01/2020.
 */

class BigDecimalConverter {

    @TypeConverter
    fun bigDecimalToDouble(input: BigDecimal?): Double {
        return input?.toDouble() ?: 0.0
    }

    @TypeConverter
    fun stringToBigDecimal(input: Double?): BigDecimal {
        if (input == null) return BigDecimal.ZERO
        return BigDecimal.valueOf(input) ?: BigDecimal.ZERO
    }

}