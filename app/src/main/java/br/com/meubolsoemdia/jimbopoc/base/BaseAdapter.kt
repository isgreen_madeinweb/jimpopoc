package br.com.meubolsoemdia.jimbopoc.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Éverdes Soares on 06/01/2020.
 */

abstract class BaseAdapter<T>(
    private val onItemClick: ((View, Int, T) -> Unit)?
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val dataList: MutableList<T> by lazy { mutableListOf<T>() }

    private var mIsLoading = false
    private var mLoadingPosition = RecyclerView.NO_POSITION

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        onCreateViewHolderBase(LayoutInflater.from(parent.context), parent, viewType)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        this.onBindViewHolderBase(holder, position)

        holder.itemView.setOnClickListener {
            onItemClick?.invoke(
                it, holder.adapterPosition,
                getItem(holder.adapterPosition)
            )
        }
    }

    abstract fun onCreateViewHolderBase(
        inflater: LayoutInflater,
        parent: ViewGroup?,
        viewType: Int
    ): RecyclerView.ViewHolder

    abstract fun <VH : RecyclerView.ViewHolder> onBindViewHolderBase(
        holder: VH,
        position: Int
    )

    override fun getItemCount() = dataList.size

    @Deprecated("Use Kotlin getter")
    fun getItem(index: Int) = dataList[index]

    val data: MutableList<T>
        get() = dataList

    val lastIndex: Int
        get() = dataList.lastIndex

    val isEmpty: Boolean
        get() = data.isEmpty()

    fun getItemRange(startIndex: Int) = getItemRange(startIndex, dataList.size)

    fun getItemRange(startIndex: Int, endIndex: Int) = dataList.subList(startIndex, endIndex)

    fun addItem(item: T) {
        dataList.add(item)
//        notifyDataSetChanged()
        notifyItemInserted(if (itemCount > 0) dataList.lastIndex else 0)
    }

    fun addItem(position: Int, item: T) {
        dataList.add(position, item)
        notifyItemInserted(position)
    }

    fun setItem(position: Int, item: T?) {
        if (item == null) {
            return
        }

        if (position > RecyclerView.NO_POSITION) {
            dataList[position] = item
        } else {
            dataList.add(item)
        }
//        notifyItemInserted(if (itemCount > 0) position else 0)
        notifyDataSetChanged()
    }

    fun changeItem(position: Int, item: T?) {
        item?.let {
            dataList[position] = item
            notifyItemChanged(position)
        }
    }

    fun removeItem(position: Int) {
        if (dataList.isEmpty()) {
            return
        }

        this.dataList.removeAt(position)
        this.notifyItemRemoved(position)
    }

    fun clearData() {
        this.dataList.clear()
        this.notifyDataSetChanged()
    }

    fun addData(list: List<T>) {
        val firstItemPosition = this.dataList.lastIndex + 1
        this.dataList.addAll(list)
        this.notifyItemRangeInserted(firstItemPosition, list.size)
//        this.notifyDataSetChanged()
    }

    fun addData(position: Int, list: List<T>) {
        this.dataList.addAll(position, list)
        this.notifyItemRangeInserted(position, list.size)
    }

    fun removeData(list: List<T>) {
        this.dataList.removeAll(list)
        this.notifyDataSetChanged()
    }

    fun showLoading(showInBottom: Boolean) {
        if (itemCount == 0) {
            return
        }

        mIsLoading = true

        mLoadingPosition = if (showInBottom) {
            addItem(getItem(0))
            lastIndex
        } else {
            addItem(0, getItem(0))
            0
        }
    }

    fun hideLoading(showInBottom: Boolean) {
        if (itemCount == 0 || mLoadingPosition == RecyclerView.NO_POSITION) {
            return
        }

        mIsLoading = false

        this.dataList.removeAt(mLoadingPosition)
        this.notifyItemRemoved(mLoadingPosition)
//        var position = 0
//
//        if (showInBottom) {
//            position = itemCount - 1
//        }
//
//        this.dataList.removeAt(position)
//        this.notifyItemRemoved(position)
    }

    fun isLoading(): Boolean {
        return mIsLoading
    }
}