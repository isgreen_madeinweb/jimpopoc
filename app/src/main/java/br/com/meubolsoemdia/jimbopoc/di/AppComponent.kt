package br.com.meubolsoemdia.jimbopoc.di

import br.com.meubolsoemdia.jimbopoc.screen.main.mainModule

/**
 * Created by Éverdes Soares on 06/01/2020.
 */

val appComponent = listOf(
    appModule,
    mainModule
)