package br.com.meubolsoemdia.jimbopoc.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import br.com.meubolsoemdia.jimbopoc.BigDecimalConverter

@Database(
    entities = [
        Card::class
    ], version = 1
)
@TypeConverters(BigDecimalConverter::class)
abstract class AppDatabase : RoomDatabase() {

    class Factory {
        companion object {
            fun create(context: Context): AppDatabase {
                return Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
//                .addMigrations(AppDatabase.MIGRATION_1_2, AppDatabase.MIGRATION_2_3)
                    .build()
            }
        }
    }

    abstract fun cardDao(): CardDao

    companion object {

        const val DATABASE_NAME = "JimboDB.db"

//        val MIGRATION_1_2: Migration = object : Migration(1, 2) {
//            override fun migrate(database: SupportSQLiteDatabase) {
//                database.execSQL("CREATE TABLE Status (" +
//                        " id INTEGER PRIMARY KEY NOT NULL, " +
//                        " type INTEGER NOT NULL, " +
//                        " taxQuantity INTEGER NOT NULL, " +
//                        " dependentsQuantity INTEGER NOT NULL, " +
//                        " date TEXT NOT NULL" +
//                        ")")
//            }
//        }
    }
}