package br.com.meubolsoemdia.jimbopoc.di

import androidx.lifecycle.LiveData

/**
 * Created by Éverdes Soares on 06/01/2020.
 */

interface BaseContract {

    interface ViewModel {
        val message: LiveData<Any>
        val loading: LiveData<Boolean>
    }

}