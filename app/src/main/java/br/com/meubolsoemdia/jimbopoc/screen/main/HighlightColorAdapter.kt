package br.com.meubolsoemdia.jimbopoc.screen.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.meubolsoemdia.jimbopoc.R
import br.com.meubolsoemdia.jimbopoc.base.BaseAdapter
import br.com.meubolsoemdia.jimbopoc.data.Card
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.activity_main_card_item.*

class CardAdapter : BaseAdapter<Card>(null) {

    override fun onCreateViewHolderBase(
        inflater: LayoutInflater,
        parent: ViewGroup?,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return CardViewHolder(
            inflater.inflate(R.layout.activity_main_card_item, parent, false)
        )
    }

    override fun <VH : RecyclerView.ViewHolder> onBindViewHolderBase(holder: VH, position: Int) {
        val card = data[position]

        val viewHolder = holder as CardViewHolder
        viewHolder.apply {
            txtCard?.text = card.alias
        }
    }

    class CardViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer

}