package br.com.meubolsoemdia.jimbopoc

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.text.Html
import android.text.TextUtils
import android.util.Base64
import android.util.DisplayMetrics
import android.util.Patterns
import java.io.*
import java.text.Normalizer
import kotlin.math.roundToInt

/**
 * Created by Éverdes Soares on 06/01/2020.
 */

class Util {

    companion object {

        fun isValidEmail(target: CharSequence): Boolean {
            return !TextUtils.isEmpty(target)
                    && Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }

        fun convertDpToPx(context: Context?, dp: Int): Int {
            return if (context != null) {
                (dp * (context.resources.displayMetrics.xdpi /
                        DisplayMetrics.DENSITY_DEFAULT)).roundToInt()
            } else {
                0
            }
        }

        fun fromHtml(htmlText: String): String {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(htmlText, Html.FROM_HTML_MODE_LEGACY).toString()
            } else {
                Html.fromHtml(htmlText).toString()
            }
        }

        fun removeAccent(text: String?): String {
            val result = Normalizer.normalize(text, Normalizer.Form.NFD)
            return result.replace("[^\\p{ASCII}\\\\“”]".toRegex(), "").trim { it <= ' ' }
        }

        fun encodeToBase64(
            path: String?,
            compressFormat: Bitmap.CompressFormat,
            quality: Int
        ): String {
            return if (path != null) {
                val image = BitmapFactory.decodeFile(path)
                val byteArrayOS = ByteArrayOutputStream()
                image.compress(compressFormat, quality, byteArrayOS)
                Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT)
            } else {
                ""
            }
        }

        @Throws(IOException::class)
        fun moveFile(inPathFile: String, outPathFile: String) {
            val outputStream = FileOutputStream(outPathFile)
            val inputStream = FileInputStream(File(inPathFile))

            val buffer = ByteArray(1024)
            var length = 0
            while ({ length = inputStream.read(buffer); length }() > 0) {
                outputStream.write(buffer, 0, length)
            }

            outputStream.flush()
            outputStream.close()
            inputStream.close()
        }

        @Throws(IOException::class)
        fun moveFile(inputStream: InputStream, outPathFile: String) {
            val outputStream = FileOutputStream(outPathFile)

            val buffer = ByteArray(1024)
            var length = 0
            while ({ length = inputStream.read(buffer); length }() > 0) {
                outputStream.write(buffer, 0, length)
            }

            outputStream.flush()
            outputStream.close()
            inputStream.close()
        }

        fun isEmpty(text: CharSequence?): Boolean {
            return text == null || text.isEmpty()
        }

        fun capitilizeWord(text: String): String {
            val words = text.split(" ").toMutableList()

            for (i in words.indices) {
                words[i] =
                    words[i].substring(0, 1).toUpperCase() + words[i].substring(1).toLowerCase()
            }

            return words.joinToString(" ")
        }
    }
}