package br.com.meubolsoemdia.jimbopoc.data

import androidx.room.Dao
import androidx.room.Query

/**
 * Created by Éverdes Soares on 06/01/2020.
 */

@Dao
interface CardDao : BaseDao<Card> {

    @Query("SELECT * FROM cards")
    suspend fun fetchAll(): List<Card>

}