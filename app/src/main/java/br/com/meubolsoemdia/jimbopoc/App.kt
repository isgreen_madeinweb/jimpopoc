package br.com.meubolsoemdia.jimbopoc

import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDexApplication
import br.com.meubolsoemdia.jimbopoc.di.appComponent
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Created by Éverdes Soares on 06/01/2020.
 */

class App : MultiDexApplication() {

    override fun onCreate() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        super.onCreate()

        startKoin {
            androidContext(this@App)
            modules(appComponent)
        }

//        if (BuildConfig.DEBUG) {
//            Stetho.initializeWithDefaults(this)
//        }
    }

}