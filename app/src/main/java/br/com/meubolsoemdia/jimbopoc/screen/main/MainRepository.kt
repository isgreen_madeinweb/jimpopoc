package br.com.meubolsoemdia.jimbopoc.screen.main

import br.com.meubolsoemdia.jimbopoc.data.AppDatabase

/**
 * Created by Éverdes Soares on 06/01/2020.
 */

class MainRepository(
//    private val preferencesHelper: PreferencesHelper
    private val appDatabase: AppDatabase
) : MainContract.Repository {

//    override suspend fun clearAuthentication() = preferencesHelper.clearAuthentication()

    override suspend fun fetchCards() = appDatabase.cardDao().fetchAll()
}