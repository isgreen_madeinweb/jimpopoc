package br.com.meubolsoemdia.jimbopoc.screen.main

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Éverdes Soares on 06/01/2020.
 */

val mainModule = module {
    factory<MainContract.Repository> { MainRepository(get()) }
    viewModel { MainViewModel(get()) }
}