package br.com.meubolsoemdia.jimbopoc.di

import br.com.meubolsoemdia.jimbopoc.data.AppDatabase
import org.koin.dsl.module

/**
 * Created by Éverdes Soares on 06/01/2020.
 */

val appModule = module {
    single { AppDatabase.Factory.create(get()) }
//    single<PreferencesHelper> { Preferences(get()) }
}