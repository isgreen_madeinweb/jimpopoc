package br.com.meubolsoemdia.jimbopoc.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.math.BigDecimal

/**
 * Created by Éverdes Soares on 06/01/2020.
 */

@Entity(tableName = "cards")
data class Card(

    @ColumnInfo @PrimaryKey(autoGenerate = true)
    val id: Long?,

    @ColumnInfo(name = "alias")
    val alias: String?,

    @ColumnInfo(name = "card_limit")
    val cardLimit: BigDecimal?,

    @ColumnInfo(name = "due_date")
    val dueDate: Int?,

    @ColumnInfo(name = "close_date")
    val closeDate: Int?,

    @ColumnInfo(name = "banner_card_id")
    val bannerCardId: Int?

)