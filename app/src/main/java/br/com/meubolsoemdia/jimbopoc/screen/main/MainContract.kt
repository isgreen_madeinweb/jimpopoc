package br.com.meubolsoemdia.jimbopoc.screen.main

import androidx.lifecycle.LiveData
import br.com.meubolsoemdia.jimbopoc.data.Card
import br.com.meubolsoemdia.jimbopoc.di.BaseContract

/**
 * Created by Éverdes Soares on 06/01/2020.
 */

interface MainContract {

    interface ViewModel : BaseContract.ViewModel {
        val cards: LiveData<List<Card>>

        fun fetchCards()
    }

    interface Repository {
        suspend fun fetchCards(): List<Card>
    }
}