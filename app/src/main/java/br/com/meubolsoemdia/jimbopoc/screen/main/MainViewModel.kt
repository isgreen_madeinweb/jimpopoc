package br.com.meubolsoemdia.jimbopoc.screen.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.meubolsoemdia.jimbopoc.data.Card
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

/**
 * Created by Éverdes Soares on 06/01/2020.
 */

class MainViewModel(
    private val repository: MainContract.Repository
) : ViewModel(), MainContract.ViewModel {

    override val message: LiveData<Any>
        get() = mMessage
    override val loading: LiveData<Boolean>
        get() = mLoading
    override val cards: LiveData<List<Card>>
        get() = mCards

    private val mMessage = MutableLiveData<Any>() // String or Int
    private val mLoading = MutableLiveData<Boolean>()
    private val mCards = MutableLiveData<List<Card>>()

    private val job = SupervisorJob()
    protected val ioScope = CoroutineScope(Dispatchers.IO + job)


    override fun fetchCards() {
        ioScope.launch {
            val cards = repository.fetchCards()
            mCards.postValue(cards)
        }
    }
}